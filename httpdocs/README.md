# Template README

## 【情報各種】

| ツール | バージョン |
| ---- | ------------------------ |
| node | v4.1.1 |
| gulp | CLI version 3.9.1 |
| sass | Sass 3.4.14 (Selective Steve) |
| HTMLテンプレートエンジン | metalsmith |

* nodeパッケージ管理は、ndenvを使用
* nodeのバージョンを合わせて作業を行なうこと

## Install Command

作業ディレクトリに移動して、下記のコマンドで環境が整います。

```
npm install
```

## Gulp Command Option

### watch task

テンプレートエンジン・Sass・画像圧縮・browserifyを実行しパブリッシュします。
吐き出し先は、public_htmlになります

```
npm start
```

then

```
gulp
```

### build task

ビルド用のタスクコマンドになります。

※スタイルガイドを修正した場合は、buildタスクから実行して下さい。

```
gulp build
```

## Metalsmith Document

| ファイル名 | 用途 |
| ---- | ------------------------ |
| _develop/◯◯.html | ページ単位 |
| _layouts/default.html | ページ共通 |
| _layouts/include/◯◯.html | インクルード用 |

* HTML静的ジェネレータ：Metalsmithを使用
* 記法：swig
* [http://www.metalsmith.io/](http://www.metalsmith.io/)
* [http://paularmstrong.github.io/swig/](http://paularmstrong.github.io/swig/)

## ローカルサーバ（browserSync）

* ローカルサーバ：browserSync
* URLポート：localhost:8001
* スタイルガイド：localhost:8001/styleguide/

## HTML

* インデントをかならず入れて下さい。（スペース：2）
* _devlopディレクトリが作業スペースになります。

| ファイル名 | 用途 |
| ---- | ------------------------ |
| layouts/ | インクルードファイル・雛形ファイル一式ディレクトリ |
| aigis-template/ | スタイルガイドのフォーマット（編集する必要なし） |
| common/ | 共通のファイル |
| その他ディレクトリ・ファイル | ページ用テンプレート |

## Sass・SCSS

| ファイル名 | 用途 |
| ---- | ------------------------ |
| common.scss | 結合用ファイル |
| _site-setting.scss | 変数 |
| _mixins.scss | ミックスイン |
| _foundation.scss | reset用（default：Normalize） |
| blocks/_◯◯.scss | module用 |

* Sassディレクトリ：common/scss
* module追加する場合は、blocks/_◯◯.scssファイルを追加してください

## 命名規則 スタイルシート

* JSなどのトリガーには、接頭辞（js-）を入れて下さい。

## Script（browserify）

| ファイル名 | 用途 |
| ---- | ------------------------ |
| main.js | サイト用のスクリプト |

* ファイルの分割は、機能ごとに分けてHTML側で読み込むスクリプトファイルにrequire()で呼び出し
* 必要なプラグイン（jQueryプラグイン等）は、npmコマンドから取得もしくはvendorの中にファイルを格納すること
* [http://browserify.org/](http://browserify.org/)

## 画像圧縮（imagemin）・画像の取り扱い

* 画像圧縮：imagemin
* ターゲットファイル：png・git・jpg
* ターゲットディレクトリ：_develop/common/images/**/*.{png,jpg,gif}
* ページ単位で使用する画像はすべて「_develop/common/images/ページディレクトリ名/*.{png,jpg,gif}」に格納すること

* Lazy load: $ npm install jquery-lazyload