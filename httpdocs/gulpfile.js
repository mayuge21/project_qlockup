'use strict';
var gulp = require('gulp'),
    $ = require('gulp-load-plugins')({
        pattern: ['gulp-*', 'gulp.*'],
        replaceString: /\bgulp[\-.]/
    });
var browserSync = require('browser-sync');
var cp = require('child_process');
var del = require('del');
var browserify = require('browserify');
var source     = require('vinyl-source-stream');
var buffer     = require('vinyl-buffer');
var runSequence  = require( 'run-sequence' ).use( gulp );

var AUTOPREFIXER_BROWSERS = {
  browsers: [
    'ie >= 10',
    'safari >= 7',
    'ios >= 7',
    'android >= 4'
  ]
};

var PATH = {
  www : './html/',
  devlop : './_develop/',
  tmp : '.tmphtml/',
  fontPath : 'common/font/',
  htmlPath : '**/*.html',
  scssPath : 'common/scss/**/*.scss',
  cssPath : 'common/css',
  imagePath :'common/images/**/*.{png,jpg,gif,svg}',
  imageDestPath : 'common/images/',
  jsPath : 'common/js/'
}
/*
 * reloadを返す関数
 */
function reload(){
  return browserSync.reload();
}
// static template engine(metalsmith)
// gulp.task('metalsmith-build', function (done) {
//   return cp.spawn('node', ['node_modules/.bin/metalsmith'], {stdio: 'inherit'})
//     .on('close', done);
// });
gulp.task('metalsmith-build', function (done) {
  return cp.spawn('npm', ['run','metalsmith'], {stdio: 'inherit'})
    .on('close', done);
});

// styleguide
gulp.task( 'guide', function () {
  return gulp.src( './aigis_config.yml' )
    .pipe( $.aigis() )
    .pipe( gulp.dest( '' ) );
});
// html
gulp.task('html', ['metalsmith-build'], function () {
  var assets = $.useref.assets({searchPath: '{html, _develop/}'});
  return gulp.src([PATH.tmp + PATH.htmlPath])
    .pipe(assets)
    .pipe($.if('*.css', $.uncss({
      html: [
        '_develop/index.html'
      ]
    })))
    .pipe($.if('*.css', $.csso()))
    .pipe(assets.restore())
    .pipe($.useref())
    .pipe(gulp.dest(PATH.www))
    .pipe($.size({title: PATH.www}));
});
// sass
gulp.task('sass', function(){
  gulp.src(PATH.devlop + PATH.scssPath)
    .pipe($.plumber({errorHandler: $.notify.onError("<%= error.message %>")}))
    .pipe($.sass())
    .pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
    .pipe(gulp.dest(PATH.www + PATH.cssPath));
});
// imagemin
gulp.task('imagemin', function() {
  var path = '_develop/' + PATH.imagePath;
  gulp.src(path)
    .pipe($.changed(PATH.www + PATH.imageDestPath))
    .pipe($.imagemin())
    .pipe(gulp.dest(PATH.www + PATH.imageDestPath));
});
// copy
gulp.task('copy', function(){
  return gulp.src([PATH.devlop + PATH.fontPath + '**'], {base: PATH.devlop}).pipe(gulp.dest(PATH.www));
});
// Browserify
function Browserify(path, dest, dest_path){
  return browserify(path, { debug: true })
    .bundle()
    .on('error', function(err){   //ここからエラーだった時の記述
        console.log(err.message);
        console.log(err.stack);
    })
    .pipe(source( dest ) )
    .pipe(buffer())
    .pipe($.sourcemaps.init( { loadMaps: true } ) )
    .pipe($.uglify() )
    .pipe($.sourcemaps.write( './' ) )
    .pipe(gulp.dest( dest_path ));
}
gulp.task('browserify', function() {
  console.log(PATH.devlop + PATH.jsPath + 'main.js');
  Browserify( PATH.devlop + PATH.jsPath + 'main.js', 'main.js', PATH.www + PATH.jsPath);
});
/* watch
 * 特定ディレクトリのファイル変更を監視し、ブラウザをリロードする
 * ただし同時に複数ファイルの変更を感知しても、リロード処理は1回のみ行う
 */
gulp.task("watch", function() {
  var timer;
  var imagesPath = '_develop/' + PATH.imagePath;
  function reloadTimer(){
    /* 連続イベントの間引き処理 200ミリ秒以内で再度発生した場合は無視する */
    /* 200ミリ秒は適当なので、必要に応じて調整 */
    clearTimeout(timer);
    timer = setTimeout(function () {
      reload();
    }, 200);
  }
  gulp.watch(PATH.www + PATH.htmlPath).on("change", function() {
    reloadTimer();
  });
  gulp.watch(PATH.devlop + PATH.fontPath).on("change", function() {
    reloadTimer();
  });
  gulp.watch(PATH.devlop + PATH.scssPath, ['sass']).on("change", function() {
    reloadTimer();
  });
  gulp.watch(imagesPath, ['imagemin']).on("change", function() {
    reloadTimer();
  });
  gulp.watch(PATH.devlop + PATH.jsPath + 'main.js', ['browserify'], PATH.www + PATH.jsPath).on("change", function() {
    reloadTimer();
  });
});
/* static server */
gulp.task("server", function() {
  browserSync.init({
    server: {
      baseDir: PATH.www,
      directory: false
    },
    notify: false,
    open: false,
//    port: 8001,  // localhost:8001
    host: '127.0.0.1'
  });
  gulp.watch([PATH.devlop + PATH.htmlPath], ['html']);
});
// clean
gulp.task('clean', del.bind(null, [PATH.tmp, PATH.www]));
gulp.task( 'build', function ( callback ) {
  runSequence( 'clean', [ 'copy', 'html', 'sass', 'imagemin', 'browserify' ], 'guide', callback );
} );
// default
// gulp.task('default', ['server', 'html', 'sass', 'imagemin', 'browserify', 'watch']);
gulp.task( 'default', function ( callback ) {
  runSequence( 'server', [ 'html', 'sass', 'imagemin', 'browserify' ], 'watch', callback );
} );
