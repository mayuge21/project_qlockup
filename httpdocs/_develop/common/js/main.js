global.jQuery = require('jquery');
var $ = require('jquery');
var easing = require('jquery-easing');
var cookie = require('./vendor/jquery.cookie.js');
var Aheight = require('./vendor/jQueryAutoHeight.js');
var lazyload = require('./vendor/jquery.lazyload.js');
/*
 ================================================================================
 * javascript information
 * file name  : main.js
 ================================================================================
 */


$(document).ready(function () {
  var $window = $(window);
  var scrWidth = 480 - (window.innerWidth - $window.width()); // 480 screen - scroll bar size

  $('.site-menu-item-list').hover(function (e) {
    if ($window.width() > scrWidth) {
      $(this).find('.site-menu-sub-wrapper').fadeIn(300);
    }
  }, function (e) {
    if ($window.width() > scrWidth) {
      $(this).find('.site-menu-sub-wrapper').hide();
    }
  });

  // Toggle the sidebar menu in small screen
  var initMenu = false;
  $('.menu-toggle-draw').click(function (e) {
    if ($window.width() <= scrWidth && !initMenu) {
      $('nav.site-menu').addClass('showing');
      initMenu = true;
      $('html').css('overflow', 'hidden');
    }
    e.stopPropagation()
  });
  $('.menu-close').click(function (e) {
    $('nav.site-menu').removeClass('showing');
    initMenu = false;
    $('html').css('overflow', 'auto');
  });
  $('nav.site-menu').click(function (e) {
    if ($window.width() <= scrWidth) {
      e.stopPropagation()
    }
  });
  $('body').click(function (e) {
    if (initMenu && $window.width() <= scrWidth) {
      $('nav.site-menu').removeClass('showing');
      initMenu = false;
      $('html').css('overflow', 'auto');
    }
  });

  // Reset status when resizing window screen
  $window.resize(function (e) {
    if ($window.width() > scrWidth) {
      $('nav.site-menu').removeClass('showing');
      initMenu = false;
      $('html').css('overflow', 'auto');

    } else {
      $('.site-menu-sub-wrapper').css({
        display: ''
      });
    }
  });

  // Stick the header when scrolling
  var initSticky = false;
  $window.scroll(function (e) {
    if (!initSticky && $window.scrollTop() > 50) {
      $('header').addClass('sticky');
      initSticky = true;
    } else if ($window.scrollTop() < 50 && initSticky) {
      $('header').removeClass('sticky');
      initSticky = false;
    }
  });

  // Dropdown menu in small screen
  if ($window.width() <= scrWidth) {
    $('li.site-menu-item-list').addClass('drop');
  }
  $('li.site-menu-item-list').children('a').click(function (e) {
    if ($window.width() <= scrWidth) {
      e.preventDefault();
      $(this).parent('li').toggleClass('drop');
    }
  });

  //for lazy load
  $(function () {
    $("img.lazy").lazyload({
      effect: "fadeIn"
    });
  });

});